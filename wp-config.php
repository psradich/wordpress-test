<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'qJ?ApS(h5R64=-kNrS|SGs&6zG`8 M&%A;(-9.$|B_|sHj^_ Tmo`La0@#|^#)U3');
define('SECURE_AUTH_KEY',  'XG~n Hez>0YY1|NeKKokZ<n<ad)bVeQ2W9nrAc~BKQ^4>k<Mo+!c}IW8m6Qn{&A_');
define('LOGGED_IN_KEY',    'cLvSf.h:=LhaFW<Yh/yLOu#b#*pG%~-aZNJdFUc(Wk;jGR.P^8wkNlo(r-k<rnA|');
define('NONCE_KEY',        '1q^u(]O[=mt>gR@nsQNU;fU7#-BiPQa:;N+}jxoqa`igge&;144CP;hf2 voGbzM');
define('AUTH_SALT',        'i6-g:Fm1lyl^wK}yVo/<@o2~LuEwXKct3M4(0+V:|pu=u.F/uma|EVr~m`Gl?4`W');
define('SECURE_AUTH_SALT', 'VD<3NBU+v{s+E2mw)CJEVaI[vqU^./d<nB+X|-2rT{,yT2&#+R|L18-@3h6K8+A!');
define('LOGGED_IN_SALT',   ')G`&pVfOqeRig}++Kz!9:;4uU~#`iYc6%o4m^Q~onVW/vEO,pm][WP-T8U43v}CT');
define('NONCE_SALT',       ' U5{KUw!XKGq(&A:+8xH*-W#<L<lOxk,C(hZk%l*<OinUYoPs=Sqf=-r#AY^o/w?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
