<?php


include_once('AeaScorecard_LifeCycle.php');

class AeaScorecard_Plugin extends AeaScorecard_LifeCycle {

    /**
     * @return array of option meta data.
     */
    public function getOptionMetaData() {
        //  http://plugin.michael-simpson.com/?page_id=31
        return array(
            //'_version' => array('Installed Version'), // Leave this one commented-out. Uncomment to test upgrades.
            'nyapikey' => array(__('NY Times API Key', 'aea_scorecard-plugin')),
            'sunlightkey' => array(__('Sunlight Labs API Key', 'aea_scorecard-plugin'))
        );
    }



    protected function initOptions() {
        $options = $this->getOptionMetaData();
        if (!empty($options)) {
            foreach ($options as $key => $arr) {
                if (is_array($arr) && count($arr > 1)) {
                    $this->addOption($key, $arr[1]);
                }
            }
        }
    }

    public function getPluginDisplayName() {
        return 'AEA Scorecard';
    }

    protected function getMainPluginFileName() {
        return 'aea-scorecard.php';
    }

    /**
     * Called by install() to create any database tables if needed.
     * @return void
     */
    protected function installDatabaseTables() {
        // @TODO maybe move DB setup to new file
        global $wpdb;
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        
        $table_name = $wpdb->prefix . 'scorecard_members';
        $sql = "CREATE TABLE $table_name (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `congID` varchar(100) DEFAULT NULL,
          `fName` varchar(100) DEFAULT NULL,
          `mname` varchar(20) DEFAULT NULL,
          `lName` varchar(100) DEFAULT NULL,
          `govTrackID` varchar(100) DEFAULT NULL,
          `score` varchar(5) DEFAULT NULL,
          `state` varchar(100) DEFAULT NULL,
          `party` varchar(100) DEFAULT NULL,
          `chamber` varchar(100) DEFAULT NULL,
          `district` varchar(100) DEFAULT NULL,
          `title` varchar(100) DEFAULT NULL,
          `congress` int(4) NOT NULL,
          `gender` varchar(2) DEFAULT NULL,
          `bio` blob,
          `image_path` varchar(255) DEFAULT NULL,
          PRIMARY KEY (`id`)
        ) ;";
     
        
        dbDelta( $sql );
        
        $table_name = $wpdb->prefix . 'scorecard_members_overwrite';
        $sql = "CREATE TABLE $table_name (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `congID` varchar(100) DEFAULT NULL,
          `congress` int(3) DEFAULT NULL,
          `fName` varchar(100) DEFAULT NULL,
          `lName` varchar(100) DEFAULT NULL,
          `image_path` varchar(255) DEFAULT NULL,
          PRIMARY KEY (`id`)
        );";
        
        dbDelta( $sql );
        
        $table_name = $wpdb->prefix . 'scorecard_key_votes';
        $sql = "CREATE TABLE $table_name (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `congress` int(10) DEFAULT NULL,
          `roll_call` varchar(40) DEFAULT '',
          `session` varchar(20) DEFAULT NULL,
          `bill_id` varchar(20) DEFAULT '',
          `chamber` varchar(20) DEFAULT '',
          `result` varchar(200) DEFAULT '',
          `vote_date` varchar(20) DEFAULT '',
          `vote_title` varchar(255) DEFAULT NULL,
          `position_summary` blob,
          `description` longblob,
          `key_vote_url` varchar(255) DEFAULT NULL,
          `question` blob,
          `position` varchar(40) DEFAULT NULL,
          `custom` varchar(100) DEFAULT 'no',
          PRIMARY KEY (`id`),
          KEY `session` (`session`)
        );";
        
        dbDelta( $sql );
        
        $table_name = $wpdb->prefix . 'scorecard_bills';
        $sql = "CREATE TABLE $table_name (
          `id` int(11) NOT NULL AUTO_INCREMENT,
            `billType` varchar(10) NOT NULL,
            `billNumber` varchar(10) NOT NULL,
            `bill_id` varchar(15) NOT NULL,
            `chamber` varchar(20) DEFAULT NULL,
            `short_title` blob NOT NULL,
            `official_title` blob NOT NULL,
            `description` longblob NOT NULL,
            `position` varchar(30) DEFAULT NULL,
            `position_summary` blob,
            `vote_date` varchar(100) NOT NULL,
            `introduced_date` varchar(100) NOT NULL,
            `key_vote_url` varchar(255) NOT NULL DEFAULT '',
            `session` varchar(4) NOT NULL,
            `summary` blob NOT NULL,
            `sponsor_position` varchar(10) DEFAULT NULL,
            `position_txt` blob,
            `issue` varchar(40) NOT NULL,
            `url` varchar(255) NOT NULL,
            `congress` varchar(4) NOT NULL,
            PRIMARY KEY (`id`),
            KEY `id` (`id`)
        );";
        
        dbDelta( $sql );
        
        $table_name = $wpdb->prefix . 'scorecard_votes';
        $sql = "CREATE TABLE $table_name (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `vote_id` varchar(20) DEFAULT NULL,
          `member` varchar(20) DEFAULT NULL,
          `congress` int(3) DEFAULT NULL,
          `vote` varchar(10) DEFAULT NULL,
          PRIMARY KEY (`id`)
        );";
        
        dbDelta( $sql );
        
        
        
        $table_name = $wpdb->prefix . 'scorecard_sponsors';
        $sql = "CREATE TABLE $table_name (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `bill_id` varchar(50) DEFAULT NULL,
          `member` varchar(30) DEFAULT NULL,
          `congress` int(4) DEFAULT NULL,
          PRIMARY KEY (`id`)
        );";
        
        dbDelta( $sql );
    }

    /**
     * Drop plugin-created tables on uninstall.
     * @return void
     */
    protected function unInstallDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('scorecard_members');
        //        $wpdb->query("DROP TABLE IF EXISTS `$tableName`");
    }


    /**
     * Perform actions when upgrading from version X to version Y
     * @return void
     */
    public function upgrade() {
    }

    public function addActionsAndFilters() {

        // Add options administration page
        add_action('admin_menu', array(&$this, 'addSettingsSubMenuPage'));

        // adding a script & style just for the options administration page
           if (strpos($_SERVER['REQUEST_URI'], $this->getSlugBase()) !== false) {
               wp_enqueue_script('aeatabs', plugins_url('/js/tabs.js', __FILE__));
               wp_enqueue_style('aeaadminstyle', plugins_url('/css/style.css', __FILE__));

        // adding a script & style for front end
           }else{

//               @TODO can this be loaded only for the plugin
               wp_enqueue_style('scorecard-fonts', "http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic");
               wp_enqueue_style('scorecard-font-awesome', "http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css") ;
               wp_enqueue_style('aealayout', plugins_url('/front-end/css/layout.css', __FILE__));
               wp_enqueue_style('aearesponsive', plugins_url('/front-end/css/responsive.css', __FILE__));
               wp_enqueue_style('selectyze', plugins_url('/front-end/css/selectyze.jquery.css', __FILE__));



           }











        // Register short codes
           $this->registerShortcodeToFunction('score-card','frontEnd');

        // Register AJAX hooks

    }
    
    
     /**
     * Handles registering the short codes
     * @return void
     */
    protected function registerShortcodeToFunction($shortcodeName, $functionName) {
        if (is_array($shortcodeName)) {
            foreach ($shortcodeName as $aName) {
                add_shortcode($aName, array($this, $functionName));
            }
        }
        else {
            add_shortcode($shortcodeName, array($this, $functionName));
        }
    }
    
    
    public function frontEnd(){

        // @TODO FINALE Make sure all JS files are compresed

        // Add for home page only
        if(strpos($_SERVER['REQUEST_URI'], "spage") == false){
            wp_enqueue_script('migrate', 'http://code.jquery.com/jquery-migrate-1.2.1.js',array('jquery'));
            wp_enqueue_script('autocomplete', plugins_url('/front-end/js/jquery.autocomplete.js', __FILE__), array('jquery'));
            wp_enqueue_script('highlightMap', plugins_url('/front-end/js/highlightMap.js', __FILE__),array('jquery'));
            wp_enqueue_script('tooltip', plugins_url('/front-end/js/jquery.tooltip.js', __FILE__), array('jquery'));
        }


        wp_enqueue_script('stickyheader', plugins_url('/front-end/js/jquery.stickyheader.js', __FILE__),array('jquery'));
        wp_enqueue_script('tablesorter', plugins_url('/front-end/js/jquery.tablesorter.js', __FILE__),array('jquery'));
        wp_enqueue_script('parsley', plugins_url('/front-end/js/parsley-IEshim.js', __FILE__), array('jquery'));
        wp_enqueue_script('selectyze', plugins_url('/front-end/js/selectyze.jquery.js', __FILE__), array('jquery'));
        wp_enqueue_script('aeamain', plugins_url('/front-end/js/main.js', __FILE__), array('jquery'));

        include('front-end/front_end_controller.php');
        $site = new front_end_controller();
        $site->siteRouter();
        
    }
}
