<?php




class AeaScorecard_actions 
{
    // Ny times API URL
    private $nyApiLocation = "http://api.nytimes.com/svc/politics/v3/us/legislative/congress/";

    // Sunlight Labs API URL
    private $sunlightApi = "http://congress.api.sunlightfoundation.com/";

    // Wrapper for $wpdb
    protected $db;
    
    public function __construct(){
        global $wpdb;
        $this->db = $wpdb;
    }

    /**
     * Sync members from given congress from api's
     * @param string $congress
     * @param string $chamber
     */
    public function membersSync($congress = "114", $chamber = "senate"){
        $this->session = $congress;
        
        // @TODO ?Can this be pulled from sunlight?
        $path = "http://api.nytimes.com/svc/politics/v3/us/legislative/congress/".$congress.'/'.$chamber."/members.json?api-key=".$this->getOption('nyapikey');
        // echo $path;
        $data = json_decode(file_get_contents($path),true);
        // print_r($data);
        $results = $data['results'][0]['members'];
        $session = $data['results'][0]['congress'];
        foreach($results as $m){
            $state = $m['state'];
            
            //Filter out no included states
            if($state != "MP" &&$state != "GU" && $state != "AS" && $state != "PR" && $state != "VI" && $state != "FM" && $state != "MH" && $state != "PW" && $state != "CZ" && $state != "PI" && $state != "TT" && $state != "CM" ) {
               
                // Get Member Data from Sulight Labs
                $sunlightPath = "http://congress.api.sunlightfoundation.com/legislators?bioguide_id=".$m['id']."&apikey=".$this->getOption('sunlightkey');
                // echo $sunlightPath."</br>";
                $memberData = json_decode(file_get_contents($sunlightPath), true);
                // print_r($memberData);
                $memberResults = $memberData['results'][0];
                
                if($memberResults){
                    $govtrack_id = $memberResults['govtrack_id'];
                    //Get The Image
                    $image_path = "http://www.govtrack.us/data/photos/".$govtrack_id."-200px.jpeg";
                    
                }else{
                    $govtrack_id = "";
                    $image_path = "";
                }
                
                
                $district = (isset($m['district'])) ? $m['district'] : "";
                $title = ($chamber == "senate") ? "Sen." : "Rep." ;
                //Build the Data Array
                $data = array(
                    'fName' => $m['first_name'],
                    'mname' => $m['middle_name'],
                    'lName' => $m['last_name'],
                    'congID' => $m['id'],
                    'govTrackID' => $govtrack_id,
                    'congress' => $session,
                    'state' => $m['state'],
                    'party' => $m['party'],
                    'district' => $district,
                    'chamber' => $chamber,
                    'gender' => "",
                    'bio' => "",
                    'title' => $title,
                    'image_path' => $image_path
                );
                //  @TODO !Remove global wpdb
                global $wpdb;
                $table = $wpdb->prefix . 'scorecard_members';
                $member = $m['id'];
                $rowCount = $wpdb->get_results("SELECT * FROM $table WHERE congID = '$member' AND congress = '$session'");
                if(count($rowCount ) == 0){
                    $wpdb->insert( $table, $data );
                }else{
                    $wpdb->update( $table, $data, array('congID' => $member, 'congress' => $session) );
                }
                
                $overtable = $wpdb->prefix . 'scorecard_members_overwrite';
                $overCount = $wpdb->get_results("SELECT * FROM $overtable WHERE congID = '$member' AND congress = '$session'");
                if(count($overCount ) != 0){
                    $overdata = array(
                        'fName' => $overCount[0]->fName,
                        'lName' => $overCount[0]->lName
                    );
                    $wpdb->update( $table, $overdata, array('congID' => $member, 'congress' => $session) );
                    // $wpdb->update( $table, $data, array('congID' => $member) );
                }
                // echo $m['id'].'<br/>';
           }    
        }
        
        $this->calc_scores($congress);
    }
    
    
    /**
    * Returns a single member
    *
    *
    * @param $id    string
    *
    * @return array
    */
    public function getMember($id){
        $table = $this->dbPrefix('scorecard_members');
        $member = $this->db->get_results("SELECT * FROM $table WHERE id = '$id'");
        return $member;
    }
    
    
    
    /**
    * Returns a all members
    *
    *
    * @param $congress    string
    *
    * @return array
    */
    public function getMembers($congress){
        $table = $this->dbPrefix('scorecard_members');
        $member = $this->db->get_results("SELECT * FROM $table WHERE congress = '$congress'");
        return $member;
    }


    /**
     * overwrites member Data to member table and member_overwrites table
     *
     * @param $congId
     * @param $congress
     * @param $data
     */
    public function overwriteMember($congId, $congress, $data){
        //  @TODO !Remove global wpdb
        global $wpdb;
        $table = $wpdb->prefix . 'scorecard_members_overwrite';
        $rowCount = $wpdb->get_results("SELECT * FROM $table WHERE congID = '$congId' AND congress = '$congress'");
        if(count($rowCount ) == 0){
            $wpdb->insert( $table, $data );
        }else{
            $wpdb->update( $table, $data, array('congID' => $congId, 'congress' => $congress) );
        }
        
        $table = $wpdb->prefix . 'scorecard_members';
        $wpdb->update( $table, $data, array('congID' => $congId, 'congress' => $congress) );
    }

    // @TODO !Need to add (yes to no) count
    // @TODO !Need to recalc Scores on vote add
    /**
     * adds a vote from api
     *
     * @param $roll_call
     * @return mixed
     */
    public function addVote($roll_call){
        $fields = 'voters,result,number,chamber,year,congress,voted_at,question,bill_id';
        $url = $this->sunlightApi.'votes?roll_id='.$roll_call.'&fields='.$fields.'&apikey='.$this->getOption('sunlightkey');
        $votes = json_decode(file_get_contents($url), true);
       
        $voteData = $votes['results'];
        $billid = (isset($voteData[0]['bill_id'])) ? $voteData[0]['bill_id'] : '';
        $data = array(
            'roll_call' => $roll_call,
            'bill_id' => $billid,
            'chamber' => $voteData[0]['chamber'],
            'congress' => $voteData[0]['congress'],
            'result' => $voteData[0]['result'],
            'vote_date' => $voteData[0]['voted_at'],
            'question' => $voteData[0]['question']
        );
        //  @TODO !Remove global wpdb
        global $wpdb;
        $table = $wpdb->prefix . 'scorecard_key_votes';
        $wpdb->insert( $table, $data ); 
        $lastid = $wpdb->get_col("SELECT ID FROM $table ORDER BY ID DESC LIMIT 0 , 1" );
        
        $congress = $voteData[0]['congress'];
        
        foreach($voteData[0]['voters'] as $v) {
            $voter = $v['voter']['bioguide_id'];
            $vote = $v['vote'];
            if($vote == "Yea") {$vote = "Yes";}
            if($vote == "Nay") {$vote = "No";}
                
             $data1 = array(
                'vote_id' => $roll_call,
                'member' => $voter,
                'vote' => $vote,
                'congress' => $congress
            );   
            $table = $wpdb->prefix . 'scorecard_votes';
            $wpdb->insert( $table, $data1 );     
        } 
        
        return $lastid[0];
    }
    
    
    /**
    * Returns a single vote
    *
    * @param $id  string
    *
    * @return array
    */
    public function getVote($id){
        $table = $this->dbPrefix('scorecard_key_votes');
        $results = $this->db->get_results("SELECT * FROM $table WHERE id = '$id' ");
        return $results;
    }
    
    
    /**
    * Returns a all votes
    *
    * @param $congress  string
    *
    * @return array
    */
    public function getVotes($congress){
        $table = $this->dbPrefix('scorecard_key_votes');
        $results = $this->db->get_results("SELECT * FROM $table WHERE congress = '$congress' ");
        return $results;
    }
    
    
    
    /**
    * Returns a member voting positions on all votes
    *
    * @param $congress  string
    * @param $member    string
    *
    * @return array
    */
    public function getMemberVotes($congress,$member){
        $table = $this->dbPrefix('scorecard_votes');
        $results = $this->db->get_results("SELECT * FROM $table WHERE congress = '$congress' AND member = '$member' ");
        return $results;
    }
    
    
    
    /**
    * Returns a member voting positions on single votes
    *
    * @param $congress  string
    * @param $member    string
    * @param $vote      string
    *
    * @return array
    */
    public function getMemberVote($congress,$member, $vote){
        $table = $this->dbPrefix('scorecard_votes');
        $results = $this->db->get_results("SELECT * FROM $table WHERE congress = '$congress' AND member = '$member'  AND vote_id = '$vote' ");
        return $results;
    }

    // @TODO !Need to recalc Scores on vote add
    /**
     * Add Bill from API
     *
     * @param $bill_id string
     *
     * @return mixed
     */
    public function addBill($bill_id){
        $fields = 'bill_type,number,congress,chamber,introduced_on,official_title,short_title,sponsor_id,cosponsor_ids';
        $url = $this->sunlightApi.'bills?bill_id='.$bill_id.'&fields='.$fields.'&apikey='.$this->getOption('sunlightkey');
        $bill = json_decode(file_get_contents($url), true);
        $billData = $bill['results'][0];

        if($billData['bill_type'] = 'hr' or $billData['bill_type'] = 'hltr'){
            $chamber = 'house';
        }else{
            $chamber = 'senate';
        }
        $data = array(
            'bill_id' => $bill_id,
            'billNumber' => $billData['number'],
            'congress' => $billData['congress'],
            'billType' => $billData['bill_type'],
            'short_title' => $billData['short_title'],
            'official_title' => $billData['official_title'],
            'introduced_date' => $billData['introduced_on'],
            'chamber' => $chamber
        );
        //  @TODO !Remove global wpdb
        global $wpdb;
        $table = $wpdb->prefix . 'scorecard_bills';
        $id = $wpdb->insert( $table, $data );
        $lastid = $wpdb->get_col("SELECT ID FROM $table ORDER BY ID DESC LIMIT 0 , 1" );
        
        $congress = $billData['congress'];
        foreach($billData['cosponsor_ids'] as $s){
            $data1 = array(
                'bill_id' => $bill_id,
                'member' => $s,
                'congress' => $congress
            );
            
            $table = $wpdb->prefix . 'scorecard_sponsors';
            $wpdb->insert( $table, $data1 );
        }
        
        
        $data2 = array(
            'bill_id' => $bill_id,
            'member' => $billData['sponsor_id'],
            'congress' => $congress
        );
        
        $table = $wpdb->prefix . 'scorecard_sponsors';
        $wpdb->insert( $table, $data2);
        
        return $lastid[0];
    }
    
    /**
    * Returns a single bill
    *
    * @param $id  string
    *
    * @return array
    */
    public function getBill($id){
        $table = $this->dbPrefix('scorecard_bills');
        $results = $this->db->get_results("SELECT * FROM $table WHERE id = '$id' ");
        return $results;
    }
    
    
    
    /**
    * Returns all bills
    *
    * @param $congress  string
    *
    * @return array
    */
    public function getBills($congress, $chamber){
        $table = $this->dbPrefix('scorecard_bills');
        if($chamber == "house"){
            $results = $this->db->get_results("SELECT * FROM $table WHERE (billType = 'hr' or billType = 'hltr') AND congress = '$congress' ");
        }else{
            $results = $this->db->get_results("SELECT * FROM $table WHERE (billType = 's' or billType = 'sltr') AND congress = '$congress' ");
        }
        return $results;
    }
    
    
    
    
    /**
    * Recaluclate member scores
    *
    * @param $congress     string
    *
    * @return void
    */
    public function calc_scores($congress){
        $members = $this->getMembers($congress);
        foreach($members as $member){
            $congID = $member->congID;
            $chamber = $member->chamber;
            $voteScore = $this->vote_score($congID,$chamber,$congress);
            $billScore = $this->bill_score($congID,$chamber,$congress);
            $score = round($voteScore*90) + round($billScore*10);
            $table = $this->dbPrefix('scorecard_members');
            $this->db->update( $table, array('score' => $score), array('congID' => $congID, 'congress' => $congress) );
        }
    }
    
    
    /**
    * Calculates the vote score for a given member
    *
    * @param $member    string
    * @param $chamber   string
    * @param $congress  string
    *
    * @return num
    */
    private function vote_score($member,$chamber,$congress){
        $votes = $this->getVotes($congress);
        $votesCast = $this->getMemberVotes($congress,$member);
        $votesCast = count($votesCast);

        $i = 0;
        $v = 0;
        foreach($votes as $vote){
            $id = $vote->roll_call;
            $pref = $vote->position;
            $mvote = $this->getMemberVote($congress,$member,$id);
            $vote = $mvote[0]->vote;
            if($pref == $vote){ $i++; }
            if($pref == null || $pref == "" || $vote == "Not Voting" || $vote == "Present" || $vote == "present") {if($votesCast) {$votesCast = $votesCast-1;}}
            $v++;
        }
        $votesRight =  $i;
        if($votesCast != 0 && $votesCast != "") {
            $rawVoteScore =  $votesRight / $votesCast;
        }else{
            $rawVoteScore = "no"; 
        }
        return $rawVoteScore;
    }
    
    
    
    /**
    * Calculates the Bill score for a given member
    *
    * @param $member    string
    * @param $chamber   string
    * @param $congress  string
    *
    * @return num
    */
    private function bill_score($member,$chamber,$congress){
        $bills = $this->getBills($congress,$chamber);
        $sponsorOp = count($bills);
        
        $j = 0;
        foreach($bills as $bill){
            $rowid = $bill->id;
            $id = $bill->bill_id;
            $pref = $bill->position;
            $bill = $this->getBill($rowid);
            $table = $this->dbPrefix('scorecard_sponsors');
            $results = $this->db->get_results("SELECT * FROM $table WHERE bill_id = '$id' AND member = '$member' AND congress = '$congress'");
            $count = count($results);
            if($count != 0 && $pref == 'Sponsor'){ $j++; }
        }
        $sonposrRight = $j;
        
        if($sponsorOp != 0 && $sponsorOp != "") {
            $rawSponsor =  $sonposrRight/$sponsorOp;
        }else{
            $rawSponsor =  0;
        }
        return $rawSponsor;
    }
    
    
    
    
    /**
    * Returns the prefixed table name
    *
    * @param $tbale  string
    *
    * @return string
    */
    private function dbPrefix($table){
        return $this->db->prefix . $table;
    }
}

