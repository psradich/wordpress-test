<?php


include_once('AeaScorecard_actions.php');
class AeaScorecard_admin_pages extends AeaScorecard_actions{

    
    /**
    * Creates HTML for the Administration page to set options for this plugin.
    *
    *
    * @return void
    */
    public function settingsPage() {
        $this->page = 'settings';

        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'aea-scorecard'));
        }
        
        // Save Settings Menu
        if(isset($_POST['current_congress'])){
            $this->updateOption('current_congress', $_POST['current_congress']);
            $this->updateOption('excerpt_length', $_POST['excerpt_length']);
            $this->success = "Your Scorecard settings have been saved!";
        }

        // Save sessions menu
        if(isset($_POST['new_session_number'])){
            $sessions = ($this->getOption('sessions')) ? $this->getOption('sessions') : array();
            $data = array(
                'number' => $_POST['new_session_number'],
                'years' => $_POST['new_session_yearOne']." - ".$_POST['new_session_yearTwo']
            );
            array_push($sessions,$data);
            usort($sessions, function($a, $b) {
                return $a['number'] - $b['number'];
            });
            $sessions = array_reverse($sessions);
            $this->updateOption('sessions', $sessions);
            $this->success = "Session #".$data['number']." has been Added";
            $this->page = 'sessions';
        }
        
        // Delete Session of congress
        if(isset($_GET['action']) && $_GET['action'] == 'delete'){
            if($_GET['source'] == 'session' && !isset($_POST['new_session_number'])){
                $val = $_GET['value'];
                $sessions = $this->getOption('sessions');
                foreach($sessions as $subKey => $subArray){
                      if($subArray['number'] == $val){
                           unset($sessions[$subKey]);
                      }
                 }
                $this->updateOption('sessions', $sessions);
                $this->success = "Session #".$val." has been deleted";
                $this->page = 'sessions';
            }
        }


        // Create and save APi menu
        $optionMetaData = $this->getOptionMetaData();
        if(isset($_POST['nyapikey'])){
            if ($optionMetaData != null) {
                foreach ($optionMetaData as $aOptionKey => $aOptionMeta) {
                    if (isset($_POST[$aOptionKey])) {
                        $this->updateOption($aOptionKey, $_POST[$aOptionKey]);
                    }
                }
                $this->page = 'api';
                $this->success = "Your API settings have been saved!";
            }
        }
        
        // Sync Members
        if(isset($_POST['syncmembers'])){
            $this->page = 'data';
            $this->membersSync('114', 'senate');
            $this->membersSync('114', 'house');

            $this->success = "Member Data has been synced!";
        }
        
        //Calc Scores
        if(isset($_POST['calcscores'])){
           $this->page = 'data';
           $this->calc_scores('114');
           $this->success = "Scores have be Recalculated!";
        }

        // HTML for the page
        $this->build('admin/settings_page', 'Settings');
        

    } // end settingsPage


    
    /**
    * Manage Members page
    * Controlls List/Edit
    *
    *
    * @return void
    */
    public function manageMembers(){

        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'aea-scorecard'));
        }
        
        // Save member edit page
        if(isset($_POST['memberPost'])){
            $data = array(
                'fName' => $_POST['fName'],
                'lName' => $_POST['lName'],
                'congID' => $_POST['congID'],
                'congress' => $_POST['congress'],
                'image_path' => $_POST['image_path']
            );
            $this->overwriteMember($_POST['congID'], $_POST['congress'], $data);
            $this->success = "Member Data has been Saved!";
        }

        
        // Create member edit view
        if(isset($_GET['action']) && $_GET['action'] == 'edit'){
            $member = $this->getMember($_GET['id']);

            // HTML for the page
            $this->build('admin/edit_member', 'Edit Member',$member[0]);
        }
        
        // Create member List view
        else{
            require_once('includes/AeaScorecard_create_table.php');
            global $wpdb;
            $table = $wpdb->prefix . 'scorecard_members';
            $listTable = new AeaScorecard_create_table();
            $listTable->prepare_items($table , array('congID', 'state', 'fName', 'lName', 'chamber', 'congress', 'score') , 20 , array('state','lName', 'chamber', 'congress', 'score') , false, true);

            // HTML for the page
            $this->build('admin/members_page', 'Members' , $listTable);
        }

    } // End Members page

    /**
    * Manage Votes Page
    * Controlls List/Create/Edit/Delete
    *
    *
    * @return void
    */
    
    public function mangeVotes(){

        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'aea-scorecard'));
        }
        
        // Add vote
        if( isset($_POST['roll_call']) ){
            
            $vote = $this->addVote( $_POST['roll_call'] );
            $this->success = "Vote ".$_POST['roll_call']." has been added";
            $data = $this->getVote($vote);
            $this->build( 'admin/edit_vote', 'Edit Key Vote',  $data[0]);
            
        }
        
        // Save Vote
        if( isset($_POST['vote_title']) ){            
            $data = array(
                'vote_title' => $_POST['vote_title'],
                'description' => $_POST['description'],
                'position_summary' => $_POST['position_summary'],
                'key_vote_url' => $_POST['key_vote_url'],
                'position' => $_POST['position']
            );
            global $wpdb;
            $id = $_POST['id'];
            $table = $wpdb->prefix . 'scorecard_key_votes';
            $wpdb->update( $table, $data, array('id' => $id) );
            $this->success = "Vote ".$_POST['vote']." has been updated";
        }
        
        
        // Delete Vote
        if(isset($_GET['action']) && $_GET['action'] == 'delete'){
            $id = $_GET['id'];
            global $wpdb;
            $table = $wpdb->prefix . 'scorecard_key_votes';
            $wpdb->query("DELETE FROM $table WHERE id = '$id'");
        }
        
        
        // Create vote edit view
        if(isset($_GET['action']) && $_GET['action'] == 'edit'){
            $id = $_GET['id'];
            
            $vote = $this->getVote($id);
            $this->build('admin/edit_vote', 'Edit Key Vote', $vote[0] );
            
        }
        
        // Create Vote list view
        elseif(!isset($_POST['roll_call'])){
            require_once('includes/AeaScorecard_create_table.php');
            global $wpdb;
            $table = $wpdb->prefix . 'scorecard_key_votes';
            $listTable = new AeaScorecard_create_table();
            $listTable->prepare_items($table , array( 'roll_call',  'congress', 'chamber') , 20 , "" , true, true);

            // HTML for the page
            $this->build('admin/votes_page', 'Key Votes', $listTable );
        }
    } // End Votes Page

    
    /**
    * Manage Bills Page
    * Controlls List/Create/Edit/Delete
    *
    *
    * @return void
    */
    public function mangeBills(){

        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'aea-scorecard'));
        }
        
        // Add bill
        if( isset($_POST['bill_num']) ){
            
            $bill = $this->addBill( $_POST['bill_num'] );
            $this->success = "Bill ".$_POST['bill_num']." has been added";
            $data = $this->getBill($bill);
            $this->build( 'admin/edit_bill', 'Edit Bill',  $data[0]);
            
        }
        
        // Save bill
        if( isset($_POST['short_title']) ){            
            $data = array(
                'short_title' => $_POST['short_title'],
                'description' => $_POST['description'],
                'position_summary' => $_POST['position_summary'],
                'key_vote_url' => $_POST['key_vote_url'],
                'position' => $_POST['position']
            );
            global $wpdb;
            $id = $_POST['id'];
            $table = $wpdb->prefix . 'scorecard_bills';
            $wpdb->update( $table, $data, array('id' => $id) );
            $this->success = "Bill ".$_POST['bill']." has been updated";
        }
        
        
        // Delete Vote
        if(isset($_GET['action']) && $_GET['action'] == 'delete'){
            $id = $_GET['id'];
            global $wpdb;
            $table = $wpdb->prefix . 'scorecard_bills';
            $wpdb->query("DELETE FROM $table WHERE id = '$id'");
        }
        
        
        // Create Bill Edit View
        if(isset($_GET['action']) && $_GET['action'] == 'edit'){
            $id = $_GET['id'];
            $bill = $this->getBill($id);
            // HTML for the page
            $this->build('admin/edit_bill', 'Edit Bill', $bill[0] );

        }
        
        // Create Bill List View
        elseif(!isset($_POST['bill_num']) && !isset($_POST['short_title'])){

            require_once('includes/AeaScorecard_create_table.php');
            global $wpdb;
            $table = $wpdb->prefix . 'scorecard_bills';
            $listTable = new AeaScorecard_create_table();
            $listTable->prepare_items($table , array('bill_id', 'short_title', 'congress') , 20 , "" , true, true);

            // HTML for the page
            $this->build('admin/bills_page', 'Bills', $listTable );
        }
    }
    

}
