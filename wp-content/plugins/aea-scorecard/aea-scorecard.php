<?php
/*
   Plugin Name: AEA Scorecard
   Version: 0.1
   Author: The Soneridge Group
   Description: Built for the American Energy Alliance, Manages and outputs the AEA Scorecard
   Text Domain: aea-scorecard
  */



$AeaScorecard_minimalRequiredPhpVersion = '5.0';

/**
 * Check the PHP version and give a useful error message if the user's version is less than the required version
 * @return boolean true if version check passed. If false, triggers an error which WP will handle, by displaying
 * an error message on the Admin page
 */
function AeaScorecard_noticePhpVersionWrong() {
    global $AeaScorecard_minimalRequiredPhpVersion;
    echo '<div class="updated fade">' .
      __('Error: plugin "AEA Scorecard" requires a newer version of PHP to be running.',  'aea-scorecard').
            '<br/>' . __('Minimal version of PHP required: ', 'aea-scorecard') . '<strong>' . $AeaScorecard_minimalRequiredPhpVersion . '</strong>' .
            '<br/>' . __('Your server\'s PHP version: ', 'aea-scorecard') . '<strong>' . phpversion() . '</strong>' .
         '</div>';
}


function AeaScorecard_PhpVersionCheck() {
    global $AeaScorecard_minimalRequiredPhpVersion;
    if (version_compare(phpversion(), $AeaScorecard_minimalRequiredPhpVersion) < 0) {
        add_action('admin_notices', 'AeaScorecard_noticePhpVersionWrong');
        return false;
    }
    return true;
}


//////////////////////////////////
// Run initialization
/////////////////////////////////




// Next, run the version check.
// If it is successful, continue with initialization for this plugin
if (AeaScorecard_PhpVersionCheck()) {
    // Only load and run the init function if we know PHP version can parse it
    include_once('aea-scorecard_init.php');
    AeaScorecard_init(__FILE__);
}
