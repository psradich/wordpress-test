<?php

include_once('front_end_model.php');
class front_end_api extends front_end_model
{

    /**
    * compiles results for the home page
    *
    * @param $congress     string
    *
    * @return array
    */
    protected function home_results($congress){
        
        $data['topHouse'] = $this->getAllResults('scorecard_members', "WHERE chamber = 'house' AND  congress = '$congress' ORDER BY score DESC LIMIT 4");
        $data['topSenate'] = $this->getAllResults('scorecard_members', "WHERE chamber = 'senate' AND  congress = '$congress' ORDER BY score DESC  LIMIT 4");
        $data['topHouseVotes'] = $this->getAllResults('scorecard_key_votes', "WHERE congress = '$congress' AND chamber = 'house' ORDER BY id DESC LIMIT 6");
        $data['topSenateVotes'] = $this->getAllResults('scorecard_key_votes', "WHERE congress = '$congress' AND chamber = 'senate' ORDER BY id DESC LIMIT 6");
        $data['membersArray'] = $this->buildMemberSuggestions($this->getAllResults('scorecard_members', "WHERE congress = '$congress'"));
        return $this->prepareDataOutput($data);
    }
    
    
    /**
    * compiles results for the Overall Results Page
    *
    * @param $congress     string
    *
    * @return array
    */
    protected function overall_results($congress){
        
        $data['members'] = $this->getAllResults('scorecard_members', "WHERE congress = '$congress' ORDER BY lName ASC");
        $data['votes'] = $this->getAllResults('scorecard_key_votes', "WHERE congress = '$congress'");
        $data['bills'] = $this->getAllResults('scorecard_bills', "WHERE congress = '$congress'");
        return $this->prepareDataOutput($data);
        
    }


    /**
     * compiles results for the State Results Page
     *
     * @param $congress  string
     * @param $state     string
     *
     * @return array
     */
    protected function state_results($congress, $state){
        
        $data['state'] = $state;
        $data['members'] = $this->getAllResults('scorecard_members', "WHERE congress = '$congress' AND state = '$state'");
        $data['votes'] = $this->getAllResults('scorecard_key_votes', "WHERE congress = '$congress'");
        $data['bills'] = $this->getAllResults('scorecard_bills', "WHERE congress = '$congress'");
        return $this->prepareDataOutput($data);
    
    }
    
    
    /**
    * compiles results for the Member Results Page
    *
    * @param $congress     string
    * @param $congID     string
    *
    * @return array
    */
    protected function member_results($congress, $congID){
        $data['member'] = $this->getAllResults('scorecard_members', "WHERE congress = '$congress' AND congID = '$congID'");
        $data['votes'] = $this->getMemberVotes($data['member'], $congress);
        return $this->prepareDataOutput($data);
    
    }


    /**
     * compiles results for a single vote Page
     *
     * @param $congress string
     * @param $voteID string
     *
     * @return array
     */
    protected function vote_results($congress, $voteID){
        $data['vote'] = $this->getAllResults('scorecard_key_votes', "WHERE id = '$voteID'");
        $data['votes'] = $this->buildVoteMembers($data['vote'], $this->congress);
        return $this->prepareDataOutput($data);
    }


    /**
     * compiles results for a single Bill Page
     *
     * @param $congress string
     * @param $billID string
     *
     * @return array
     */
    protected function bill_results($congress, $billID){
        $data['bill'] = $this->getAllResults('scorecard_bills', "WHERE id = '$billID'");
        $data['members'] = $this->buildBillMembers($data['bill'], $congress);

        return $this->prepareDataOutput($data);
    }
}