<?php


include_once('front_end_api.php');
class front_end_controller extends front_end_api
{

    // @TODO !make dynamic
    public $congress = '114';

    /**
     * Main router for the frontEnd
     */
    public function siteRouter(){
        $page = (isset($_GET['spage'])) ? $_GET['spage'] : "";
        switch ($page) {
            case 'overall':
                $this->overall_page();
                break;
            case 'member':
                $this->member_page();
                break;
            case 'state':
                $this->state_page();
                break;
            case 'vote':
                $this->vote_page();
                break;
            case 'bill':
                $this->bill_page();
                break;
            default:
                $this->home_page();
        }
        
    }


    /**
     * Generate Home Page
     */
    public function home_page(){
        $data = $this->home_results($this->congress);
        $this->build('home_view',$data);
        
    }

    /**
     * Generate Overall Results Page
     */
    public function overall_page(){
        $data = $this->overall_results($this->congress);
        $this->build('overall_view', $data);
    }


    /**
     * Generate Member Page
     */
    public function member_page(){
        $congID = $_GET['id'];
        $data = $this->member_results($this->congress, $congID);
        $this->build('member_view',$data);
    }


    /**
     * Generate State Page
     */
    public function state_page(){
        $state = $_GET['state'];
        $data = $this->state_results($this->congress, $state);
        $this->build('state_view', $data);
    }

    /**
     * Generate Single Vote Page
     */
    public function vote_page(){
        $voteID = $_GET['id'];
        $data = $this->vote_results($this->congress, $voteID);
        $this->build('vote_view', $data);
    }

    /**
     * Generate Single Bill Page
     */
    public function bill_page(){
        $billID = $_GET['id'];
        $data = $this->bill_results($this->congress, $billID);
        $this->build('bill_view', $data);
    }
    
}