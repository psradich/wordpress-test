<?php



class front_end_core
{
    
    // Wrapper for $wpdb
    private $db;

    // @TODO ? Should really be dynamic
    // WP OPtions prefix
    private $optionPrefix = "AeaScorecard_Plugin_";
    
    public function __construct(){
        
        global $wpdb;
        $this->db = $wpdb;
        
    }
    
    /**
    * Return array from DB with optional query modifers
    *
    * @param $table     string
    * @param $query     string
    *
    * @return array
    */
    protected function getAllResults($table = "", $query = "") {
        if($table) {
            $tablename = $this->dbPrefix($table);
            return $this->db->get_results("SELECT * FROM $tablename $query");
        }else{
            die('getAllResults() $table can not be empty');
        }
    }


    /**
     * Return array from DB for given fields with optional query modifers
     *
     * @param  $table string
     * @param  $fields array
     * @param  $query  string
     *
     * @return mixed
     */
    protected function getResults($table = "", $fields = array(), $query = ""){
        if( $table ) {
            $tablename = $this->dbPrefix($table);
            $fields_length = count($fields);
            if ($fields_length > 0) {
                $fieldString = "";
                $i = 1;
                foreach ($fields as $f) {
                    $fieldString .= $f;
                    if ($i < $fields_length) {
                        $fieldString .= ",";
                    }
                    $i++;
                }
                return $this->db->get_results("SELECT $fieldString FROM $tablename $query");
            } else {
                die('getResults() $fields array can not be empty');
            }
        }else{
            die('getResults() $table can not be empty');
        }
    }


    /**
     * return a query from the DB
     *
     * @param  $query  string
     * @return mixed
     */
    protected function getQuery( $query = "" ){

        if( $query ){
            return $this->db->get_results($query);
        }else{
            die('getQuery() $query can not be empty');
        }
    }



    /**
    * Returns the prefixed table name
    *
    * @param $tbale  string
    *
    * @return string
    */
    protected function dbPrefix($table){
        return $this->db->prefix . $table;
    }
    
    
    
    
    /**
     * A wrapper function delegating to WP get_option() but it prefixes the input $optionName
     * to enforce "scoping" the options in the WP options table thereby avoiding name conflicts
     * 
     * @param $optionName string defined in settings.php and set as keys of $this->optionMetaData
     * @param $default string default value to return if the option is not set
     * 
     * @return string the value from delegated call to get_option(), or optional default value
     * if option is not set.
     */
    public function getOption($optionName, $default = null) {
        $prefixedOptionName = $this->optionPrefix.$optionName;
        $retVal = get_option($prefixedOptionName);
        if (!$retVal && $default) {
            $retVal = $default;
        }
        return $retVal;
    }
    
    
    
     /**
     * Includes files from the /views folder, also passes in $data for filling
     * out views.  $view van be a single filename without the .php extension
     *
     * @param $view  string
     * @param $data  array
     */
    function build($view = "", $data = array()){
        if($view) {
            $imgpath = $this->imagePath();
            include(plugin_dir_path(__FILE__) . '/views/' . $view . '.php');
        }else{
            die('build() $view can not be empty');
        }
    }
    
    
    /**
     * Includes files from the /views/parts folder, also passes in $data for filling
     * out views.
     * 
     * @param $view string
     * @param $data array
     *
     */
    function templatePart($view, $data = array()){
        $imgpath = $this->imagePath();
        include(plugin_dir_path(__FILE__)."/views/parts/".$view.".php");
    }

    /**
     * ruturns image path for templates
     *
     * @return string
     */
    private function imagePath(){
        return plugins_url('aea-scorecard')."/front-end/images/";
    }



    /**
     * Get current page url minus Query string
     *
     * @returns       string
     * 
     */
    function curPageURL() {
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
         $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
         $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        $pageURL = explode("?", $pageURL);
        return $pageURL[0];
    }


    /**
     * Generates site links
     *
     * @param $page     string
     * @param $string string
     *
     * @return string
     */
    public function getLink($page, $string = ""){
        $url = $this->curPageURL();
        return $url."?spage=".$page.$string;
    }


    /**
     * Changes date format
     *
     * @param $date  string
     * @param  $format  string
     *
     * @return string
     */
    public function changeDateFormat($date, $format = 'Y-m-d'){
        return date($format, strtotime($date));
    }

    /**
     * Returns full name of state
     *
     * @param $state
     * @return mixed
     */
    public function translateStateName($state){
        include_once('assets/states_array.php');
        $state = strtoupper($state);
        return $states[$state];
    }


    /**
     * Take the data need for the page and append data needed for every page
     *
     * @param  $data array  The $data array need for the page
     *
     * @return array
     */
    public function prepareDataOutput($data = array()){
        if(!isset($data['sessions'])) {
            $data['sessions'] = $this->getOption('sessions');
        }

        return $data;
    }


    /**
     * Generate Excerpt of text
     *
     * @param  $text string
     * @param  $numb string
     *
     * @return string
     */
    public function exerpt($text = "", $numb = "80"){
        if($this->getOption('excerpt_length')){$numb = $this->getOption('excerpt_length');}
        if (strlen($text) > $numb) {
            $text = substr($text, 0, $numb);
            $text = substr($text,0,strrpos($text," "));
            $etc = " ...";
            $text = $text.$etc;
        }
        return $text;
    }
}