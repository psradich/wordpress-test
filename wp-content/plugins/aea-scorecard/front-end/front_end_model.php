<?php


include_once('front_end_core.php');
class front_end_model extends front_end_core
{


    /**
     * Generates json for member name suggestions
     *
     * @param  $members array
     *
     * @return json
     */
    public function buildMemberSuggestions($members = array()){
        $membersArray = array();
        foreach($members as $m){
            $membersArray[$m->congID] = $m->fName." ".$m->lName;
        }
        return json_encode($membersArray);
    }


    /**
     * Compile the members that are and are not on a given bill
     *
     * @param  $bill  array
     * @param  $congress  string
     *
     * @return mixed
     */
    public function buildBillMembers($bill = array(), $congress = ""){
        $chamber = $bill[0]->chamber;
        $billID = $bill[0]->bill_id;
        $members = $this->getAllResults('scorecard_members', "WHERE congress = '$congress' AND chamber = '$chamber'");
        $sponsors = $this->getAllResults('scorecard_sponsors', "WHERE bill_id = '$billID'");
        $onBill = array();
        $notOnBill = array();
        foreach($members as $m) {
            foreach ($sponsors as $ob) {
                if($m->congID == $ob->member) {
                    array_push($onBill, $m);
                }else {
                    array_push($notOnBill, $m);
                }
            }
        }
        $data['sponsorCount'] = count($sponsors);
        $data['onBill'] = $onBill;
        $data['notOnBill'] = $notOnBill;
        return $data;
    }


    /**
     * Compile list of Members for a given vote
     *
     * @param  $vote array
     * @param  $congress string
     *
     * @return mixed
     */
    public function buildVoteMembers( $vote = array(), $congress = "" ){
        $voteID = $vote[0]->roll_call;
        $memtable = $this->dbPrefix('scorecard_members');
        $votesTable = $this->dbPrefix('scorecard_votes');
        $data['members'] = $this->getQuery(
            "SELECT *
            FROM $memtable, $votesTable
            WHERE $votesTable.vote_id = '$voteID'
            AND $votesTable.member = $memtable.congID
            AND $memtable.congress = '$congress'
            ORDER BY $memtable.score DESC, $memtable.lName ASC");

        return $data;
    }


    /**
     * Compile votes for a given member
     *
     * @param  $member array
     * @param  $congress string
     *
     * @return mixed
     */
    public function getMemberVotes($member = array(), $congress = ""){
        $congID = $member[0]->congID;
        $chamber = $member[0]->chamber;
        $votesTable = $this->dbPrefix('scorecard_votes');
        $KeyVotesTable = $this->dbPrefix('scorecard_key_votes');

        $votes = $this->getQuery("
            SELECT *, $KeyVotesTable.id as voteID
            FROM $KeyVotesTable, $votesTable
            WHERE $KeyVotesTable.chamber = '$chamber'
            AND $KeyVotesTable.congress = '$congress'
            AND $votesTable.vote_id = $KeyVotesTable.roll_call
            AND $votesTable.member = '$congID'");


        foreach($votes as $v){
            echo $v->id;
        }

        return $votes;
    }
}

