$(document).ready(function()
{
    // @TODO FINALE Document and orginize this file
	//Run on Init
	setBtnInnerWidth();
	setEqualHeights();
	setGraphFill();
    // NEEDS TO RUN TWICE!!!
	setVertMiddle(); setVertMiddle();
	
	
	//Run on Scroll or Resize
	$(window).resize(function(){ setBtnInnerWidth(); setEqualHeights(); setGraphFill(); setVertMiddle();});
	$(window).scroll(function(){ });
	
	//Custom SelectBox
	$('.searchList').Selectyze({
		theme : 'AEA'
	});


    $('.resultsTable tr.link').click(function(){
        var link = $(this).data('link');
        window.location = link;
    });

	
	$(".resultsTable").tablesorter();
	$(".resultsTable").stickyTableHeaders();
	
	$(".tabsBar li").click(function(){
		$('.overtable').addClass('hide');
		var table = $(this).data('table');
		$('.'+table).removeClass('hide');
		$(".tabsBar li").removeClass('active');
		$(this).addClass('active');
	});


    $('.tip').click(function(){
        var url = $(".mapLink").attr('rel') + $(this).attr('rel');
        window.location = url;
    })




    $('.nameSearch').submit(function(){
        window.location = $(this).data('link')+$('#autofill').data('congID');
        return false;
    });
	
});


// Set Width of Button inner table cell
function setBtnInnerWidth() {
	$( ".btn" ).each(function() {
		var btnWidth = $(this).width()
		$('.btnInner', this).width(btnWidth);
	});
};


function setEqualHeights() {
	$('.setEqualHeights').each(function(){
		var currentTallest = 0;
		$(this).children('div').css({'min-height': 0});
		$(this).children('div').each(function(){
			if ($(this).outerHeight() > currentTallest) { currentTallest = $(this).outerHeight(); }
		});
		$(this).children('div').css({'min-height': currentTallest});
	});
};

// Set Fill of bar graphs
function setGraphFill() {
	$( ".graphFill" ).each(function() {
		var score = $(this).attr('data-score');
		$('.score', this).html(score +'%')
		if ($(window).width() > 480 ) {
			$(this).css({'width': score+'%'});
		}
		
		if ($(this).outerWidth() <= 230) {
			$('.score', this).css({'float': 'left'});
		} else {
			$('.score', this).css({'float': 'right'});
		};
	});
};

// vertMiddle - Set vertical alignment to middle (can calculate the height for table-cell style vert middle or calculate for absolute style)
function setVertMiddle() {
	$( ".vertMiddle" ).each(function() {
		var boxType = $(this).css('display');
		if (boxType == 'table-cell') {
			var innerHeight = ($(this).parent('.rowInner').css('min-height'));
			$(this).css('height', innerHeight)
		}else{
			var vMargin = $(this).height() / 2;
			$(this).css('margin-top', '-' + vMargin + 'px').css('top', '50%').css('position', 'absolute');
		}
	});
}



function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}