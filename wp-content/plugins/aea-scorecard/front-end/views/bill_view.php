<div id="scoreCardWrapper">
    <?php $bill = $data['bill'][0]; ?>
    <?php $this->templatePart("controls_row", $data); ?>

    <div id="contentRow" class="row">
        <div class="rowInner">

            <div id="infoBar">
                <div id="infoPath">2013-2014 <i class="fa fa-angle-double-right"></i> <a href="#">House</a></div>
                <div id="infoSocial">
                    <a href="#"><i class="fa fa-print"></i></a>
                    <a href="#"><i class="fa fa-envelope"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                </div>
                <br class="clear">
            </div> <!-- End infoBar -->

            <h3><?php echo $bill->short_title; ?> <br>Bill <?php echo $bill->billType.$bill->billNumber; ?></h3>

            <div id="billDetail">
                <div class="contentCol">
                    <h4>Description</h4>
                    <p><?php echo $bill->description; ?></p>
                    <br>
                    <h4>AEA Action Position</h4>
                    <p><?php echo $bill->position_summary; ?></p>
                    <br>
                </div> <!-- End contentCol -->

                <div class="sideCol">
                    <div id="positionBlock">
                        <div class="position">AEA&nbsp;Position: <span class="upper"><?php echo $bill->position; ?></span></div>
                        <div class="positionData">
                            <h3><?php echo  $data['members']['sponsorCount']; ?> </h3>
                            Sponsored this bill
                        </div>
                        <a class="btn btnOrange" href="<?php echo $bill->key_vote_url; ?>"><div class="btnInner">SEE KEY VOTE ALERT</div></a>

                    </div><!-- End positionBlock -->
                </div> <!-- End sideCol -->

                <br class="clear">
            </div>

            <ul class="tabsBar">
                <li class="active" data-table="onBillTable"><div>On The Bill</div></li>
                <li data-table="notOnBillTable"><div>NOT ON THE BILL</div></li>
            </ul>
            <div class="scrollTitle">
                <h3>Bill <?php echo $bill->short_title; ?></h3>
            </div>


            <div class="onBillTable overtable">
                <?php $this->templatePart("members_table", $data['members']['onBill']); ?>
            </div>

            <div class="notOnBillTable hide overtable">
                <?php $this->templatePart("members_table", $data['members']['notOnBill']); ?>
            </div>

        </div> <!--END ROWINNER -->
    </div> <!-- END ROW -->


    <?php $this->templatePart("footer_form"); ?>


    <br class="clear">
</div>