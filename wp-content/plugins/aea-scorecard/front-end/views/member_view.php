<div id="scoreCardWrapper">
    <?php $this->templatePart("controls_row", $data); ?>
	
	
	<div id="contentRow" class="row">
		<div class="rowInner">
	    
		<div id="infoBar">
			<div id="infoPath">2013-2014 <i class="fa fa-angle-double-right"></i> <a href="#">House</a> <i class="fa fa-angle-double-right"></i> <a href="#">Georgia</a> <i class="fa fa-angle-double-right"></i> <a href="#">Republican</a></div>
		    <div id="infoSocial">
			<a href="#"><i class="fa fa-print"></i></a>
			<a href="#"><i class="fa fa-envelope"></i></a>
			<a href="#"><i class="fa fa-facebook"></i></a>
			<a href="#"><i class="fa fa-twitter"></i></a>
		    </div>
			<br class="clear">
		</div> <!-- End infoBar -->
		
			
		<div id="repDetail">
			
		    <div id="repPhoto">
			<div class="photo"><img src="<?php echo $data['member'][0]->image_path; ?>"></div>
			<div class="compare"><i class="fa fa-check-circle-o"></i>&nbsp;&nbsp;Compare</div>
		    </div>
		    
		    <div id="repInfo">
			<h3><?php echo $data['member'][0]->title." ".$data['member'][0]->fName." ".$data['member'][0]->lName; ?></h3>
			<p>(<?php echo $data['member'][0]->party; ?>) <?php echo $data['member'][0]->state; ?>&nbsp;&nbsp;&nbsp; <?php if($data['member'][0]->district) { echo "District ".$data['member'][0]->district; } ?></p>
		    </div>
		    
		    
		    
		    <div id="repGraphs">
			<div class="currentScore barGraph">
				<div class="graphFill" data-score='<?php echo $data['member'][0]->score; ?>'>
				<div class="data vertMiddle"><div class="label">Current Score</div><div class="score"></div></div>
			    </div>
			</div>
            <!--  @TODO ! calc and store chamber averages                -->
			<div class="avgScore barGraph">
				<div class="graphFill" data-score='40'>
				<div class="data vertMiddle"><div class="label">House Average</div><div class="score"></div></div>
			    </div>
			</div>
		    </div>
			
			
		    
		    <br class="clear">
		</div>

		<ul class="tabsBar">
			<li class="active"><div>Key Votes</div></li>
		    <li><div>Co-Sponsorships</div></li>
		</ul>

            <?php $this->templatePart("member_votes_table", $data['votes']); ?>
		   
	    
	    </div> <!--END ROWINNER -->
	</div> <!-- END ROW -->


    <?php $this->templatePart("footer_form"); ?>
	    
	
	<br class="clear">
</div>