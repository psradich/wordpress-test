<div id="scoreCardWrapper">
    <?php $this->templatePart("controls_row", $data); ?>
	
	
	<div id="contentRow" class="row">
		<div class="rowInner">
	    
		<div id="infoBar">
			<div id="infoPath">2013-2014 <i class="fa fa-angle-double-right"></i> <a href="#">Breadcrumb</a></div>
		    <div id="infoSocial">
			<a href="#"><i class="fa fa-print"></i></a>
			<a href="#"><i class="fa fa-envelope"></i></a>
			<a href="#"><i class="fa fa-facebook"></i></a>
			<a href="#"><i class="fa fa-twitter"></i></a>
		    </div>
			<br class="clear">
		</div> <!-- End infoBar -->
		
		<h3>Overall Results</h3>
            <script>

                $(function(){
//                    param getParameterByName('sw');
                    if(getParameterByName('sw') == 'votes'){
                        $('.overtable').addClass('hide');
                        $('.votesTable').removeClass('hide');
                        $(".tabsBar li").removeClass('active');
                        $("li[data-table='votesTable']").addClass('active');
                    }

                    if(getParameterByName('sw') == 'bills'){
                        $('.overtable').addClass('hide');
                        $('.billsTable').removeClass('hide');
                        $(".tabsBar li").removeClass('active');
                        $("li[data-table='billsTable']").addClass('active');
                    }
                })

            </script>
		<ul class="tabsBar overallTabs">
			<li class="active" data-table="membersTable"><div>MEMBERS</div></li>
			<li data-table="votesTable"><div>VOTES</div></li>
			<li data-table="billsTable"><div>co-sponsorships</div></li>
		</ul>

        <div class="scrollTitle">
            <h3>Overall Results</h3>
        </div>

		<div class="membersTable overtable">
			<?php $this->templatePart("members_table", $data['members']); ?>
		</div>
		
		<div class="votesTable hide overtable">
			<?php $this->templatePart("votes_table", $data['votes']); ?>
		</div>

		<div class="billsTable hide overtable">
			<?php $this->templatePart("bills_table", $data['bills']); ?>
		</div>
		
	    </div> <!--END ROWINNER -->
	</div> <!-- END ROW -->


    <?php $this->templatePart("footer_form"); ?>
	    
	
	<br class="clear">
</div>