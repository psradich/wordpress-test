<div id="subscribeRow" class="row">
    <div class="rowInner">
        <!-- @TODO !Hook up footer form        -->
        <form method="post" action="#" data-validate="parsley">
            <label>Never miss a vote.</label>
            <input name="name" placeholder="Name" class="field" type="text" data-required="true">
            <input name="email" placeholder="Email" class="field" type="text" data-required="true" data-type="email">
            <input name="submit" type="submit" value="subscribe" class="btn btnDarkBlue">
            <br class="clear">
        </form>

    </div> <!--END ROWINNER -->
</div> <!-- END ROW -->