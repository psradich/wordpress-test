<table class="resultsTable  repTable" width="100%" border="0" cellspacing="0" cellpadding="0">
    <thead> 
        <tr class="titleRow">
        <th class="stateCell">VOTE # <span class="sort"><i class="fa fa-sort"></i></span> <span class="sortup"><i class="fa fa-sort-asc"></i></span> <span class="sortdown"><i class="fa fa-sort-desc"></i></span></th>
        <th class="districtCell">TITLE <span class="sort"><i class="fa fa-sort"></i></span> <span class="sortup"><i class="fa fa-sort-asc"></i></span> <span class="sortdown"><i class="fa fa-sort-desc"></i></span></th>
        <th class="nameCell">DESCRIPTION <span class="sort"><i class="fa fa-sort"></i></span> <span class="sortup"><i class="fa fa-sort-asc"></i></span> <span class="sortdown"><i class="fa fa-sort-desc"></i></span></th>
        <th class="aeaPosCell">AEA POSITION <span class="sort"><i class="fa fa-sort"></i></span> <span class="sortup"><i class="fa fa-sort-asc"></i></span> <span class="sortdown"><i class="fa fa-sort-desc"></i></span> </th>
        <th class="repPosCell">REP VOTED <span class="sort"><i class="fa fa-sort"></i></span> <span class="sortup"><i class="fa fa-sort-asc"></i></span> <span class="sortdown"><i class="fa fa-sort-desc"></i></span></th>
        </tr>
  </thead>
  <tbody>   
        <!-- @TODO ! Need to have a desctiption excerpt       -->
         <?php
            foreach($data as $m){
            ?>
                                  
                <tr class="link " data-link="<?php echo $this->getLink('vote', "&id=".$m->voteID); ?>">
                    <td class="voteCell"><?php echo $m->roll_call; ?></td>
                    <td class="titleCell"><?php echo $m->vote_title ?></td>
                    <td class="descripCell"><?php echo $this->exerpt($m->description); ?></td>
                    <td class="aeaPosCell"><?php echo $m->position; ?></td>
                    <td class="repPosCell"><?php echo $m->vote; ?></td>
                </tr>
                                  
             <?php
            }
          ?>
  </tbody> 	
</table>

