<table class="resultsTable overallTable" width="100%" border="0" cellspacing="0" cellpadding="0">
    <thead> 
        <tr class="titleRow">
        <th class="stateCell">STATE <span class="sort"><i class="fa fa-sort"></i></span> <span class="sortup"><i class="fa fa-sort-asc"></i></span> <span class="sortdown"><i class="fa fa-sort-desc"></i></span></th>
        <th class="districtCell">DISTRICT <span class="sort"><i class="fa fa-sort"></i></span> <span class="sortup"><i class="fa fa-sort-asc"></i></span> <span class="sortdown"><i class="fa fa-sort-desc"></i></span></th>
        <th class="nameCell">NAME <span class="sort"><i class="fa fa-sort"></i></span> <span class="sortup"><i class="fa fa-sort-asc"></i></span> <span class="sortdown"><i class="fa fa-sort-desc"></i></span></th>
        <th class="partyCell">PARTY <span class="sort"><i class="fa fa-sort"></i></span> <span class="sortup"><i class="fa fa-sort-asc"></i></span> <span class="sortdown"><i class="fa fa-sort-desc"></i></span></th>
        <th class="scoreCell">SCORE <span class="sort"><i class="fa fa-sort"></i></span> <span class="sortup"><i class="fa fa-sort-asc"></i></span> <span class="sortdown"><i class="fa fa-sort-desc"></i></span></th>
        </tr>
  </thead>
  <tbody>   
    
         <?php
            foreach($data as $m){
            ?>
                                  
                <tr class="link" data-link="<?php echo $this->getLink('member', "&id=".$m->congID); ?>">
                    <td class="stateCell"><?php echo $m->state; ?></td>
                    <td class="districtCell"><?php if($m->district) { echo "District ".$m->district;} ?></td>
                    <td class="nameCell"><?php echo $m->title." ".$m->fName." ".$m->lName; ?></td>
                    <td class="partyCell"><?php echo $m->party; ?></td>
                    <td class="scoreCell"><?php echo $m->score; ?>%</td>
                </tr>
                                  
             <?php
            }
          ?>
  </tbody> 	
</table>