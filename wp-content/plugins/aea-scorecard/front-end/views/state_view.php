<div id="scoreCardWrapper">

    <?php $this->templatePart("controls_row", $data); ?>

    <div id="contentRow" class="row">
        <div class="rowInner">

            <div id="infoBar">
                <div id="infoPath">2013-2014 <i class="fa fa-angle-double-right"></i> <a href="#">Georgia</a></div>
                <div id="infoSocial">
                    <a href="#"><i class="fa fa-print"></i></a>
                    <a href="#"><i class="fa fa-envelope"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                </div>
                <br class="clear">
            </div> <!-- End infoBar -->
            <?php $stateName = $this->translateStateName($data['state']); ?>
            <h3>State Results: <span class="stateName"><?php echo $stateName;  ?></span></h3>


            <div class="scrollTitle">
                <h3>State Results: <span class="stateName"><?php echo $stateName; ?></span></h3>
            </div>
            <?php $this->templatePart("members_table", $data['members']); ?>

        </div> <!--END ROWINNER -->
    </div> <!-- END ROW -->


    <?php $this->templatePart("footer_form"); ?>


    <br class="clear">
</div>