<div id="scoreCardWrapper">
    <?php $vote = $data['vote'][0]; ?>
    <?php $this->templatePart("controls_row", $data); ?>

    <div id="contentRow" class="row">
        <div class="rowInner">

            <div id="infoBar">
                <div id="infoPath">2013-2014 <i class="fa fa-angle-double-right"></i> <a href="#">House</a></div>
                <div id="infoSocial">
                    <a href="#"><i class="fa fa-print"></i></a>
                    <a href="#"><i class="fa fa-envelope"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                </div>
                <br class="clear">
            </div> <!-- End infoBar -->

            <h3><?php echo $vote->vote_title; ?> <br>Vote <?php echo $vote->roll_call; ?></h3>

            <div id="voteDetail">
                <div class="contentCol">
                    <h4>Description</h4>
                    <p><?php echo $vote->description; ?></p>
                    <br>
                    <h4>AEA Action Position</h4>
                    <p><?php echo $vote->position_summary; ?></p>
                    <br>
                </div> <!-- End contentCol -->

                <div class="sideCol">
                    <div id="positionBlock">
                        <div class="position">AEA&nbsp;Position: <span><?php echo $vote->position; ?></span></div>
                        <div class="positionData">
                            <span>Vote result on <?php echo $this->changeDateFormat($vote->vote_date); ?></span>
                            <h3><?php echo $vote->result; ?></h3>
                            147 to 278
                        </div>
                        <a class="btn btnOrange" href="<?php echo $vote->key_vote_url; ?>"><div class="btnInner">SEE KEY VOTE ALERT</div></a>

                    </div><!-- End positionBlock -->
                </div> <!-- End sideCol -->

                <br class="clear">
            </div>
            <div id="scrollPoint"></div>
            <!--  @TODO !! Remove JS from here  -->
            <script>
                $(function(){
                    $('.NoTR').hide();
                    $('.VotingTR').hide();
                    $('.PresentTR').hide();

                    $('.noVote').click(function(){

                        $('.YesTR').hide();
                        $('.VotingTR').hide();
                        $('.PresentTR').hide();
                        $('.NoTR').show();
                        $('html, body').animate({
                            scrollTop: $("#scrollPoint").offset().top
                        }, 500);
                    });

                    $('.yesVote').click(function(){

                        $('.NoTR').hide();
                        $('.VotingTR').hide();
                        $('.PresentTR').hide();
                        $('.YesTR').show();
                        if($('.tabsBar').hasClass('turn')) {
                            $('html, body').animate({
                                scrollTop: $("#scrollPoint").offset().top
                            }, 500);
                        }
                    })

                    $('.notVote').click(function(){

                        $('.NoTR').hide();
                        $('.YesTR').hide();
                        $('.VotingTR').show();
                        $('.PresentTR').show();
                        $('html, body').animate({
                            scrollTop: $("#scrollPoint").offset().top
                        }, 500);
                    })
                })
            </script>

            <ul class="tabsBar voteTabs">
                <li class="active yesVote"><div>VOTED YES</div></li>
                <li class="noVote"><div>VOTED NO</div></li>
                <li class="notVote"><div>DID NOT VOTE</div></li>
            </ul>

            <div class="scrollTitle">
                <h3><?php echo $vote->vote_title; ?></h3>
            </div>

            <?php $this->templatePart("vote_members_table", $data['votes']['members']); ?>

        </div> <!--END ROWINNER -->
    </div> <!-- END ROW -->


    <?php $this->templatePart("footer_form"); ?>


    <br class="clear">
</div>