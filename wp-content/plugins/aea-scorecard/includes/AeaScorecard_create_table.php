<?php


if( ! class_exists( 'WP_List_Table' ) ) {
   require_once( 'wp_list_table.php' );
}
 
/**
 * Create a new table class that will extend the WP_List_Table
 */
class AeaScorecard_create_table extends WP_List_Table
{
    
    public $table;
    
    public $canDelete;
    
    public $canEdit;
    
    /**
     * Prepare the items for the table to process
     *
     * @return Void
     */
    public function prepare_items($table, $cols = '', $perPage = 20, $sort = "", $canDelete = true, $canEdit = true)
    {
        
        $this->table = $table;
        $this->canDelete = $canDelete;
        $this->canEdit = $canEdit;
        
        $columns = $this->get_columns($table, $cols);
        $sortable = $this->get_sortable_columns($sort);
 
        $data = $this->table_data($table, $cols);
        usort( $data, array( &$this, 'sort_data' ) );
 
        $perPage = $perPage;
        $currentPage = $this->get_pagenum();
        $totalItems = count($data);
 
        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ) );
 
        $data = array_slice($data,(($currentPage-1)*$perPage),$perPage);
 
        $this->_column_headers = array($columns, '', $sortable);
        $this->items = $data;
    }
 
    /**
     * Override the parent columns method. Defines the columns to use in your listing table
     *
     * @return Array
     */
    public function get_columns($table, $cols)
    {
        
       global $wpdb;
       $colnames = $wpdb->get_results("SHOW COLUMNS FROM $table");
       
       $columns = array();
       if($this->canDelete){
            $columns['cb'] = '<input type="checkbox" />';
       }
        
       foreach($colnames as $col){
           if(is_array($cols)){
               if(in_array($col->Field, $cols)){
                   $columns[$col->Field] = $col->Field;
               }
           }else{
               $columns[$col->Field] = $col->Field;
           }
       }
        
        // print_r($columns);
        return $columns;
    }
 
    
 
    /**
     * Define the sortable columns
     *
     * @return Array
     */
    public function get_sortable_columns($sortable)
    {
        $n = array();
        if(is_array($sortable)){
            foreach($sortable as $s){
                $n[$s] = array($s,false);
            }
       
        }  
        return $n;
        // return array('chamber' => array('chamber', false), 'state' => array('state', false));
    }
 
    /**
     * Get the table data
     *
     * @return Array
     */
    private function table_data($table, $cols)
    {
        $data = array();
        global $wpdb;
       // $table = 'wp_scorecard_members';
       if(isset($_POST['s'])){
           $s = $_POST['s'];
           $columns = $this->get_columns($table, $cols);
           unset($columns['cb']);
           $like = "WHERE ";
           $i = 1;
           foreach($columns as $c){
               if($i > 1){
                   $like .= " OR ";
               }
               $like .= $c." LIKE '%".$s."%'";
               $i++;
           }
           $dbdata = $wpdb->get_results("SELECT * FROM $table $like");           
       }else{
            $dbdata = $wpdb->get_results("SELECT * FROM $table ");
       }
       foreach($dbdata as $d){
           $f = array();
           foreach($d as $k => $v){
              $f[$k] = $v;
           }
           $data[] = $f;
       }
       return $data;
    }
 
    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        return $item[ $column_name ];
    }
 
    /**
     * Allows you to sort the data by the variables set in the $_GET
     *
     * @return Mixed
     */
    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'title';
        $order = 'asc';
 
        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }
 
        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }
 
 
        $result = strnatcmp( $a[$orderby], $b[$orderby] );
 
        if($order === 'asc')
        {
            return $result;
        }
 
        return -$result;
    }
    
    
    function column_id($item) {
        if($this->canEdit){    
            $actions['edit'] = sprintf('<a href="?page=%s&action=%s&id=%s">Edit</a>',$_REQUEST['page'],'edit',$item['id']);
        }
        
        if($this->canDelete){ 
            $actions['delete'] = sprintf('<a href="?page=%s&action=%s&id=%s">Delete</a>',$_REQUEST['page'],'delete',$item['id']);
        }
      
    
        return sprintf('%1$s %2$s', $item['id'], $this->row_actions($actions) );
    }
    
   function column_congID($item) {
        if($this->canEdit){    
            $actions['edit'] = sprintf('<a href="?page=%s&action=%s&id=%s">Edit</a>',$_REQUEST['page'],'edit',$item['id']);
        }
        
        if($this->canDelete){ 
            $actions['delete'] = sprintf('<a href="?page=%s&action=%s&id=%s">Delete</a>',$_REQUEST['page'],'delete',$item['id']);
        }
      
    
        return sprintf('%1$s %2$s', $item['congID'], $this->row_actions($actions) );
    }
   
   
   function column_roll_call($item) {
        if($this->canEdit){    
            $actions['edit'] = sprintf('<a href="?page=%s&action=%s&id=%s">Edit</a>',$_REQUEST['page'],'edit',$item['id']);
        }
        
        if($this->canDelete){ 
            $actions['delete'] = sprintf('<a href="?page=%s&action=%s&id=%s">Delete</a>',$_REQUEST['page'],'delete',$item['id']);
        }
      
    
        return sprintf('%1$s %2$s', $item['roll_call'], $this->row_actions($actions) );
    }
    
    function column_bill_id($item) {
        if($this->canEdit){    
            $actions['edit'] = sprintf('<a href="?page=%s&action=%s&id=%s">Edit</a>',$_REQUEST['page'],'edit',$item['id']);
        }
        
        if($this->canDelete){ 
            $actions['delete'] = sprintf('<a href="?page=%s&action=%s&id=%s">Delete</a>',$_REQUEST['page'],'delete',$item['id']);
        }
      
    
        return sprintf('%1$s %2$s', $item['bill_id'], $this->row_actions($actions) );
    }
    
   
       
    function get_bulk_actions() {
      if($this->canDelete){      
          //$actions = array(
          //  'delete'    => 'Delete'
          //);
          //return $actions;
      }
    }
    
    function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="book[]" value="%s" />', $item['id']
        );    
    }
}