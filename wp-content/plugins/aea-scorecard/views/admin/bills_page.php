<div class="wrap">
           

    <h2><?php echo $this->getPluginDisplayName(); echo ' - '; _e($title, 'aea-scorecard'); ?> <a href="#" class="add-new-h2 addNewVote">Add Bill</a></h2>
    
    <?php if(isset($this->success)){ ?>
        <div class="updated"><p><?php echo $this->success; ?></p></div>
    <?php } ?>
    
    <div class="addVote">
        <form method="post" action="" class="voteAddForm">
            <input type="text" name="bill_num" value="" id="bill_num" class="input-large rc" placeholder="Bill # (example hr1030-114)"/>
            <br /> <br />
            <input type="submit" class="button-primary" value="<?php _e('Add Bill', 'aea-scorecard') ?>"/>
        </form>
    </div>
    
    <form method="post">
        <input type="hidden" name="page" value="AeaScorecard_Plugin_members" />
        <?php $data->search_box('search', 'search_id'); ?>
    
        <?php $data->display(); ?>
    </form> 

</div>