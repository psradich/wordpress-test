<div class="wrap">
    
    <h2><?php echo $this->getPluginDisplayName(); echo ' - '; _e($title, 'aea-scorecard'); ?> </h2>
    
    <?php if(isset($this->success)){ ?>
        <div class="updated"><p><?php echo $this->success; ?></p></div>
    <?php } ?>
    
          <?php wp_enqueue_media(); ?>
    <hr />
    <form method="post" action="">
        <h2><strong><?php echo $data->title." ".$data->fName." ".$data->lName; ?></strong> - <?php echo $data->congress; ?>th Congress</h2>
        <h3>(<?php echo $data->party; ?>) - <?php echo $data->state; ?> <?php if($data->district) { echo " - District ".$data->district ;} ?></h3>
        <br />
        <div class="left memImage">
            <img src="<?php echo $data->image_path; ?>" alt="" class="memberImage" /><br />
            <input type="button" name="upload-btn" id="upload-btn" class="button-secondary" value="Change Image">
        </div>
        
        
        <div class="left memEdit">
            <table class="form-table"><tbody> 
                <tr valign="top">
                    <th scope="row"><p><label for="fName">First Name</label></p></th>
                    <td>
                    <input type="text" name="fName" value="<?php echo $data->fName; ?>" id="fName" class="regular-text"/>
                    </td>
                </tr>
                
                <tr valign="top">
                    <th scope="row"><p><label for="lName">Last Name</label></p></th>
                    <td>
                    <input type="text" name="lName" value="<?php echo $data->lName; ?>" id="fName" class="regular-text"/>
                    </td>
                </tr>
            </tbody></table>
            
        </div>
        
        <br class="clear" />
        
        
        
        <input type="hidden" name="memberPost" value="1" id="memberPost"/>
        <input type="hidden" name="congress" value="<?php echo $data->congress; ?>" id="congress"/>
        <input type="hidden" name="congID" value="<?php echo $data->congID; ?>" id="congID"/>
        <input type="hidden" name="image_path" value="<?php echo $data->image_path; ?>" id="image_path"/>
            
      
       



         <p class="submit">
            <input type="submit" class="button-primary"
                   value="<?php _e('Save Changes', 'aea-scorecard') ?>"/>
        </p>
        
    </form>

</div>



<script type="text/javascript">
jQuery(document).ready(function($){
    $('#upload-btn').click(function(e) {
        e.preventDefault();
        var image = wp.media({ 
            title: 'Upload Image',
            // mutiple: true if you want to upload multiple files at once
            multiple: false
        }).open()
        .on('select', function(e){
            // This will return the selected image from the Media Uploader, the result is an object
            var uploaded_image = image.state().get('selection').first();
            // We convert uploaded_image to a JSON object to make accessing it easier
            // Output to the console uploaded_image
            console.log(uploaded_image);
            var image_url = uploaded_image.toJSON().url;
            // Let's assign the url value to the input field
            $('#image_url').val(image_url);
            $('.memberImage').attr('src',image_url);
            $('#image_path').val(image_url)
        });
    });
});
</script>