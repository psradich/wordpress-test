<div class="wrap">
           

    <h2><?php echo $this->getPluginDisplayName(); echo ' - '; _e($title, 'aea-scorecard'); ?></h2>
    
    <?php if(isset($this->success)){ ?>
        <div class="updated"><p><?php echo $this->success; ?></p></div>
    <?php }else{ ?>
        <br /><br />
    <?php } ?>
   
    <h2 class="nav-tab-wrapper settingTabs">
        <a href="#" class="nav-tab  <?php if($this->page == 'settings'){echo "nav-tab-active"; } ?>" data-tab="settings">Scorecard Settings</a>
        <a href="#" class="nav-tab  <?php if($this->page == 'data'){echo "nav-tab-active"; } ?>" data-tab="data">Data Sync</a>
        <a href="#" class="nav-tab <?php if($this->page == 'sessions'){echo "nav-tab-active"; } ?>" data-tab="sessions">Sessions of Congress</a>
        <a href="#" class="nav-tab <?php if($this->page == 'api'){echo "nav-tab-active"; } ?>" data-tab="api">API Settings</a>
    </h2>
    
    <div class="settings tabpage <?php if($this->page == 'settings'){echo "active"; } ?>">
        <form method="post" action="">
            <table class="form-table"><tbody>      
                <tr valign="top">
                    <th scope="row"><p><label for="current_congress">Current Congress</label></p></th>
                    <td>
                    <select name="current_congress" id="">
                        <?php foreach($this->getOption('sessions') as $s){
                            $selected = ($s['number'] == $this->getOption('current_congress')) ? "selected" : "";
                        ?>    
                            <option value="<?php echo $s['number']; ?>" <?php echo $selected; ?> ><?php echo $s['number']." &nbsp;&nbsp;&nbsp;".$s['years']; ?></option>    
                        <?php } ?>
                    </select> 
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row"><p><label for="excerpt_length">Descrption Exerpt Length</label></p></th>
                    <td>
                        <input type="text" name="excerpt_length" value="<?php echo $this->getOption('excerpt_length'); ?>" id="current_congress" class="small-text"/>
                        <span class="description">Number of characters (leave blank for default)</span>
                    </td>
                </tr>
            </tbody></table>
            <p class="submit">
                <input type="submit" class="button-primary"
                       value="<?php _e('Save Changes', 'aea-scorecard') ?>"/>
            </p>
       </form>
    </div>
      
    <div class="data tabpage <?php if($this->page == 'data'){echo "active"; } ?>">
       <br /><br />
       <form method="post" action="">
           <input type="hidden" name="syncmembers" value="" id="some_name"/>
           <input type="submit" class="button-primary"
                       value="<?php _e('Sync Member Data', 'aea-scorecard') ?>"/>
       </form>
       <br><br>
       <form method="post" action="">
           <input type="hidden" name="calcscores" value="" id="some_name"/>
           <input type="submit" class="button-primary"
                       value="<?php _e('Recalculate Scores', 'aea-scorecard') ?>"/>
       </form> 
    </div>
    
    
    
    <div class="sessions tabpage <?php if($this->page == 'sessions'){echo "active"; } ?>">
        <br /><br />
        <h1>Seesions <a href="#" class="add-new-h2 addSession">Add Session</a></h1>
        
        <?php 
        foreach($this->getOption('sessions') as $s){ ?>
            <div class="congSessions">
                <div class="left congSessiondelete">
                    <a href="?page=<?php echo $_REQUEST['page']; ?>&action=delete&source=session&value=<?php echo $s['number']; ?>"><span class="dashicons dashicons-dismiss"></span></a>
                </div>
                <div class="left congSessionNum">
                    <?php echo $s['number']; ?>
                </div>
                
                <div class="left congSessionYears">
                    <?php echo $s['years']; ?>
                </div>
                
                <br class="clear" />
            </div>
    
        <?php } ?>
        
        
        
        <form method="post" action="" class="sessionForm">
            <table class="form-table"><tbody>      
                <tr valign="top">
                    <th scope="row"><p><label for="new_session_number">New Session Number</label></p></th>
                    <td>
                    <input type="text" name="new_session_number" value="<?php echo $this->getOption('new_session_number'); ?>" id="current_congress" class="small-text"/>
                    </td>
                </tr>
                
                <tr valign="top">
                    <th scope="row"><p><label for="new_session_yearOne">New Session Years</label></p></th>
                    <td>
                    <input type="text" name="new_session_yearOne" value="<?php echo $this->getOption('new_session_yearOne'); ?>" id="current_congress" class="small-text"/> - 
                    <input type="text" name="new_session_yearTwo" value="<?php echo $this->getOption('new_session_yearTwo'); ?>" id="current_congress" class="small-text"/>
                    </td>
                </tr>
            </tbody></table>
            <p class="submit">
                <input type="submit" class="button-primary"
                       value="<?php _e('Save Changes', 'aea-scorecard') ?>"/>
            </p>
       </form>
    </div>
    
    
    
    <div class="api tabpage <?php if($this->page == 'api'){echo "active"; } ?>">
       <form method="post" action="">
            <table class="form-table"><tbody>      
                <?php
                $optionMetaData = $this->getOptionMetaData();
                if ($optionMetaData != null) {
                    foreach ($optionMetaData as $aOptionKey => $aOptionMeta) {
                        $displayText = is_array($aOptionMeta) ? $aOptionMeta[0] : $aOptionMeta;
                        ?>
                            <tr valign="top">
                                <th scope="row"><p><label for="<?php echo $aOptionKey ?>"><?php echo $displayText ?></label></p></th>
                                <td>
                                <?php $this->createFormControl($aOptionKey, $aOptionMeta, $this->getOption($aOptionKey)); ?>
                                </td>
                            </tr>
                        <?php
                    }
                }
                ?>
            </tbody></table>
            <p class="submit">
                <input type="submit" class="button-primary"
                       value="<?php _e('Save Changes', 'aea-scorecard') ?>"/>
            </p>
       </form>  
    </div>
           
</div>





