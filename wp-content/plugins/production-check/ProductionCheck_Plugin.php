<?php


include_once('ProductionCheck_LifeCycle.php');

class ProductionCheck_Plugin extends ProductionCheck_LifeCycle {

    /**
     * See: http://plugin.michael-simpson.com/?page_id=31
     * @return array of option meta data.
     */
    public function getOptionMetaData() {
        //  http://plugin.michael-simpson.com/?page_id=31
        return array(
            //'_version' => array('Installed Version'), // Leave this one commented-out. Uncomment to test upgrades.
            'ATextInput' => array(__('Enter in some text', 'my-awesome-plugin')),
            'Donated' => array(__('I have donated to this plugin', 'my-awesome-plugin'), 'false', 'true'),
            'CanSeeSubmitData' => array(__('Can See Submission data', 'my-awesome-plugin'),
                                        'Administrator', 'Editor', 'Author', 'Contributor', 'Subscriber', 'Anyone')
        );
    }

//    protected function getOptionValueI18nString($optionValue) {
//        $i18nValue = parent::getOptionValueI18nString($optionValue);
//        return $i18nValue;
//    }

    protected function initOptions() {
        $options = $this->getOptionMetaData();
        if (!empty($options)) {
            foreach ($options as $key => $arr) {
                if (is_array($arr) && count($arr > 1)) {
                    $this->addOption($key, $arr[1]);
                }
            }
        }
    }

    public function getPluginDisplayName() {
        return 'Production Check';
    }

    protected function getMainPluginFileName() {
        return 'production-check.php';
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Called by install() to create any database tables if needed.
     * Best Practice:
     * (1) Prefix all table names with $wpdb->prefix
     * (2) make table names lower case only
     * @return void
     */
    protected function installDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('mytable');
        //        $wpdb->query("CREATE TABLE IF NOT EXISTS `$tableName` (
        //            `id` INTEGER NOT NULL");
    }

    /**
     * See: http://plugin.michael-simpson.com/?page_id=101
     * Drop plugin-created tables on uninstall.
     * @return void
     */
    protected function unInstallDatabaseTables() {
        //        global $wpdb;
        //        $tableName = $this->prefixTableName('mytable');
        //        $wpdb->query("DROP TABLE IF EXISTS `$tableName`");
    }


    /**
     * Perform actions when upgrading from version X to version Y
     * See: http://plugin.michael-simpson.com/?page_id=35
     * @return void
     */
    public function upgrade() {
    }

    public function addActionsAndFilters() {

        // Add options administration page
        // http://plugin.michael-simpson.com/?page_id=47
//        add_action('admin_menu', array(&$this, 'addSettingsSubMenuPage'));

        // Example adding a script & style just for the options administration page
        // http://plugin.michael-simpson.com/?page_id=47
                if (strpos($_SERVER['REQUEST_URI'], $this->getSettingsSlug()) !== false) {
//                    wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));
//                    wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
                }else{
                    $jsvars = array();

                    if( strpos(file_get_contents(get_template_directory()."/footer.php"),'GoogleAnalyticsObject') !== false) {
                        $jsvars['analytics'] = 'true';
                    }else{
                        if( strpos(file_get_contents(get_template_directory()."/header.php"),'GoogleAnalyticsObject') !== false) {
                            $jsvars['analytics'] = 'true';
                        }else{
                            if($this->getOption('has_analytics') == "true"){
                                $jsvars['analytics'] = 'true';
                            }else{
                                $jsvars['analytics'] = 'false';
                            }

                        }
                    }

                    if( strpos(file_get_contents(get_template_directory()."/footer.php"),'google_conversion_id') !== false) {
                        $jsvars['remarket'] = 'true';
                    }else{
                        if( strpos(file_get_contents(get_template_directory()."/header.php"),'google_conversion_id') !== false) {
                            $jsvars['remarket'] = 'true';
                        }else{
                            if($this->getOption('has_remarketing') == "true"){
                                $jsvars['remarket'] = 'true';
                            }else{
                                $jsvars['remarket'] = 'false';
                            }

                        }
                    }




                    wp_register_script( 'mainjs', plugins_url('/js/main.js', __FILE__), array('jquery') );
                    wp_localize_script( 'mainjs', 'dir', $jsvars );
                    wp_enqueue_script( 'mainjs' );
                }


        // Add Actions & Filters
        // http://plugin.michael-simpson.com/?page_id=37


        // Adding scripts & styles to all pages
        // Examples:
        //        wp_enqueue_script('jquery');
        //        wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
        //        wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));


        // Register short codes
        // http://plugin.michael-simpson.com/?page_id=39


        // Register AJAX hooks
        // http://plugin.michael-simpson.com/?page_id=41

    }


}
