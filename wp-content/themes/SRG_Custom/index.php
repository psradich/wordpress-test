<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query. 
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

            <div class="main">
    			<?php
    			//Loop each blog post
                if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                    <div class="post">
                        <span><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></span>
                        <?php the_excerpt(); ?>
                        <img src="<?php bloginfo('template_directory'); ?>/images/wordpress.png" />
                    </div>
                    
                <?php
                
                endwhile; else:
                // No posts.
                endif;
                wp_reset_query();
                ?>
            </div>



        <?php
     query_posts( array( 'paged' => get_query_var('paged') ) );
     if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
     <div class="homePost">
          <div class="date left">
               <?php the_time('M') ?><br/><?php the_time('d') ?>
              
          </div><!-- end date -->
         
          <div class="homePostInner left">
               <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
               <?php echo excerpt(100); ?>
          </div><!-- end homePostInner -->
    
           <br class="clear"/>
          
     </div><!-- end homepoat -->    
    
     <?php
     endwhile; else:
     // No posts.
     endif;
     if(function_exists('wp_paginate')) {
     wp_paginate();
     }
     wp_reset_query();
     ?>

    <?php get_template_part('search', 'box'); ?>


<?php get_sidebar(); ?>
<?php get_footer(); ?>